#!/usr/bin/env python3

import time
import random
import sys


def main():
    # TODO!!!
    print()


def checkorder(arr):
    for i in range(len(arr)-1):
        if arr[i] > arr[i+1]:
            raise ValueError("Wrong order: " + str(arr[i]) + " > " +
                             str(arr[i+1]) + " index " + str(i))


def insertionsort(arr):
    if len(arr) < 2:
        return
    for i in range(1, len(arr)):
        b = arr[i]
        j = i - 1
        while j >= 0:
            if arr[j] <= b:
                break
            arr[j+1] = arr[j]
            j -= 1
        arr[j+1] = b


def binsertionsort(arr):
    if len(arr) < 2:
        return
    for i in range(1, len(arr)):
        b = arr[i]
        left = 0
        right = i
        while left < right:
            mid = int((left + right) / 2)
            if b < arr[mid]:
                right = mid
            else:
                left = mid + 1
        arr[left+1:i+1] = arr[left:i]
        arr[left] = b


def quicksort(arr, left, right):
    if (right - left) < 2:
        return
    x = arr[int((left + right) / 2)]
    i = left
    j = right - 1
    while i < j:
        while arr[i] < x:
            i += 1
        while arr[j] > x:
            j -= 1
        if i > j:
            break
        tmp = arr[i]
        arr[i] = arr[j]
        arr[j] = tmp
        i += 1
        j -= 1
    if left < j:
        quicksort(arr, left, j + 1)
    if i < right - 1:
        quicksort(arr, i, right)


def mergesort(arr, left, right):
    if len(arr) < 2 or (right - left) < 2:
        return
    k = int((left + right)/2)
    mergesort(arr, left, k)
    mergesort(arr, k, right)
    merge(arr, left, k, right)


def merge(arr, left, k, right):
    if len(arr) < 2 or (right - left) < 2 or k <= left or k >= right:
        return
    tmp = [0] * (right - left)
    n1 = left
    n2 = k
    m = 0
    while True:
        if n1 < k and n2 < right:
            if arr[n1] > arr[n2]:
                tmp[m] = arr[n2]
                n2 += 1
            else:
                tmp[m] = arr[n1]
                n1 += 1
            m += 1
        else:
            if n1 >= k:
                tmp[m:] = arr[n2:right]
            else:
                tmp[m:] = arr[n1:k]
            break
    arr[left:right] = tmp[:]


def bubblesort(arr):
    if len(arr) < 2:
        return
    unsorted = True
    while unsorted:
        unsorted = False
        for i in range(1, len(arr)):
            if arr[i-1] > arr[i]:
                unsorted = True
                tmp = arr[i-1]
                arr[i-1] = arr[i]
                arr[i] = tmp


def selectionsort(arr):
    if len(arr) < 2:
        return
    for m in range(0, len(arr) - 1):
        minind = m
        minel = arr[m]
        for i in range(m+1, len(arr)):
            if arr[i] < arr[minind]:
                minind = i
                minel = arr[i]
        arr[m+1:minind+1] = arr[m:minind]
        arr[m] = minel


if __name__ == '__main__':
    main()

